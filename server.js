var express = require("express");
var path = require("path");
var app = express();

app.use("/", express.static(path.join(__dirname, "/dest")));

// app.get("/", function(req, res) {
//    res.sendFile("index.html");
// })

app.listen(4001, function() {
  console.log("listen on 4001!");
});