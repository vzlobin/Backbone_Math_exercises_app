require.config({
    paths: {
      jquery: "../lib/jquery",
      underscore: "../lib/underscore",
      backbone: "../lib/backbone",
      handlebars: "../lib/handlebars"
    }
  });
  
  require(["app"], function (App) {
    App.initialize();
  });
  